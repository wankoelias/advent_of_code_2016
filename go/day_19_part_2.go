package main

import "fmt"

func main() {
    var count int = 3014387
	var elfes []int
	var current int = 0

	
	for i:= 0; i<count; i++ {
	
		elfes = append(elfes, i)
	}
	
	
	
	for count > 1 {
		
		opposite := (current + count / 2) % count
		elfes = append(elfes[:opposite], elfes[opposite + 1:]...)
		count = count - 1
		
		if opposite > current {
			current = current + 1
		}
		
		if current == count {
			current = 0
		}
		
		if count % 1000 == 0 {
			fmt.Println(len(elfes))
		}		
	
	}
	
	fmt.Println(elfes)
}