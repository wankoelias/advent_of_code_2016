data = "01110110101001000"
size = 35651584

while len(data) < size:
    data = data+"0"+"".join(["0" if d == "1" else "1" for d in data[::-1]])

data = data[:size]
check = ""

while True:
    for j in xrange(0, len(data)-1, 2):
        check += "1" if data[j] == data[j + 1] else "0"

    if len(check) % 2 != 0:
        break

    data = check
    check = ""

print check
