import re
input = open("day09_input").read()
global i
def decompress(string):

    i+=1
    if not "(" in string:
        return len(string)
    else:
        current = re.findall("^([^\(]+)?\((\d+)x(\d+)\)(.*)", string)[0]
        newstring = current[3][:int(current[1])]
        remaining = current[3][int(current[1]):]
        count = len(current[0])
        multiplier = int(current[2])

        if not "(" in newstring:
            count += len(newstring)*multiplier
        else:
            count += decompress(newstring)*multiplier

        if remaining:
            return count + decompress(remaining)
        else:
            return count


print decompress(str(input))


""" part 1 """

current = ""
decompressed = ""
while input:

    current = re.findall("^([^\(]+)?\((\d+)x(\d+)\)(.*)", input)[0]
    decompressed += current[0]+current[3][:int(current[1])]*int(current[2])
    input = current[3][int(current[1]):]


print(len(decompressed))

