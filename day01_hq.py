__author__ = 'Elias'
import copy

input = open("day01_input").read().replace(" ", "").split(",")


pos = {"x":0, "y":0}
dirc = 0

locs = []

for i in input:

    print "input: {}".format(i)

    if i[0] == "R":
        dirc += 1
    else:
        dirc -= 1

    if dirc == -1:
        dirc = 3

    if dirc == 4:
        dirc = 0

    print "direction: {}".format(dirc)

    step = int(i[1:])
    print "step: {}".format(step)

    for _ in range(step):

        if pos in locs:
            break
        locs.append(copy.copy(pos))

        if dirc == 0:
            pos["y"] += 1
        elif dirc == 1:
            pos["x"] += 1
        elif dirc == 2:
            pos["y"] -= 1
        elif dirc == 3:
            pos["x"] -= 1

    print "position: {}\n".format(pos)

print "distance: {}".format(abs(pos["x"])+abs(pos["y"]))