""" part 1
triangles = [x for x in [[int(l) for l in line.split(" ") if l] for line in open("day03_input").read().split("\n")] if
            x[0]+x[1] > x[2] and
            x[0]+x[2] > x[1] and
            x[1]+x[2] > x[0]]
"""
import numpy as np

triangles = np.swapaxes(np.array([[int(l) for l in line.split(" ") if l] for line in open("day03_input").read().split("\n")]), 0, 1)
count = 0
for row in triangles:
    for i in range(0, len(row), 3):
        x = row[i:i+3]

        if x[0]+x[1] > x[2] and x[0]+x[2] > x[1] and x[1]+x[2] > x[0]:
            count += 1


print str(count)