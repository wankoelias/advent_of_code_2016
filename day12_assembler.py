inst = [x.split() for x in open("day12_input").read().split("\n")]
pos = 0
a, b, c, d = 0, 0, 1, 0

while pos < len(inst):
    print "a: {} b: {} c: {} d: {}".format(a, b, c, d)
    if inst[pos][0] == "cpy":
        exec(inst[pos][2] + "=" + inst[pos][1])
    elif inst[pos][0] == "inc":
        exec(inst[pos][1] + "+=1")
    elif inst[pos][0] == "dec":
        exec(inst[pos][1] + "-=1")
    elif inst[pos][0] == "jnz":
        if eval(inst[pos][1]) != 0:
            pos += int(inst[pos][2])
            continue
    pos += 1


print "a: {} b: {} c: {} d: {}".format(a, b, c, d)