import hashlib

def xrow(string, x, comp=False):
    for i in range(len(string)-(x-1)):
        chk = comp if comp else string[i]
        xr = string[i:i + x]

        if xr.count(chk) == x:
            return chk

    return False

salt = "yjdafjpo"
indices = []
i = 0

buffer = []

def extent_buffer(count):
    for i in xrange(len(buffer), len(buffer)+count+1):
        hashing = salt+str(i)
        for j in xrange(2017):
            hashing = hashlib.md5(hashing).hexdigest()
        buffer.append(hashing)

extent_buffer(1000)

while len(indices) < 64:
    current = buffer[i]
    triple = xrow(current, 3)

    if triple:
        if triple in [xrow(h, 5, triple) for h in buffer[i+1:i+1001]]:
            indices.append(current)
            print str((len(indices), i))
    i+=1
    extent_buffer(1)

