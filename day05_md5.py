import hashlib

id = "reyedfim"
password = {}

index = 0
while len(password.keys()) < 8:
    hash_c = hashlib.md5(id+str(index)).hexdigest()
    if hash_c[:5] == "00000" and hash_c[5] not in password.keys() and hash_c[5] in [str(x) for x in range(8)]:
        password[hash_c[5]] = hash_c[6]
        print "partial password: " + str(password)
    index += 1

print "full password: " + "".join([x[1] for x in sorted(password.items())])

