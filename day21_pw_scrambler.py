import re

data = open("day21_input").read().split("\n")
data.reverse()
pw_ = "fbgdceah"

pw = list(pw_)

print pw_

for line in data:
    parts = line.split()

    if parts[0] == "swap" and parts[1] == "position":
        i1 = int(parts[2])
        i2 = int(parts[5])

        p1 = pw[i1]
        p2 = pw[i2]

        pw[i1] = p2
        pw[i2] = p1

    elif parts[0] == "swap" and parts[1] == "letter":

        p1 = parts[2]
        p2 = parts[5]

        i1 = pw.index(p1)
        i2 = pw.index(p2)

        pw[i1] = p2
        pw[i2] = p1

    elif parts[0] == "reverse":
        i1 = int(parts[2])
        i2 = int(parts[4])

        a = pw[i1:i2+1]
        a.reverse()
        pw[i1:i2+1] = a

    elif parts[0] == "move":
        i2 = int(parts[2])
        i1 = int(parts[5])

        if i1 < i2:
            pw = pw[:i1]+pw[i1+1:i2+1]+[pw[i1]]+pw[i2+1:]
        else:
            pw = pw[:i2] + [pw[i1]] + pw[i2:i1] + pw[i1+1:]

    elif parts[0] == "rotate" and parts[1] in ["left", "right"]:

        steps = int(parts[2])

        for _ in range(steps):
            pw = [pw[-1]] + pw[:-1] if parts[1] == "left" else pw[1:] + [pw[0]]

    else:

        counter = {0:-1, 1:-1, 2:2, 3:-2, 4:1, 5:-3, 6:0, 7:-4}

        steps = counter[int(pw.index(parts[6]))]

        dir = "l" if steps < 0 else "r"

        for _ in range(abs(steps)):
            pw = [pw[-1]] + pw[:-1] if dir == "r" else pw[1:] + [pw[0]]

    print line, "".join(pw)

print "".join(pw)