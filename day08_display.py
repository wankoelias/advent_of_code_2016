import numpy as np
import re
inputs = open("day08_input").read().split("\n")

class Screen:

    def __init__(self, dsize):
        self.display = np.zeros(shape=dsize, dtype=int)

    def count(self):
        return np.sum(self.display)

    def rotate(self, direction, position, amount):
        if direction == "column":
            slice = self.display[:,position].copy()
        elif direction == "row":
            slice = self.display[position, :].copy()

        slice_ = slice.copy()
        slice_[amount:] = slice[:-amount]
        slice_[:amount] = slice[-amount:]

        if direction == "column":
            self.display[:,position] = slice_
        elif direction == "row":
            self.display[position, :] = slice_

        pass

    def rect(self, a, b):
        self.display[:b, :a] = 1


display_size = (6, 50)
myscreen = Screen(display_size)


for input in inputs:
    command = [i for i in re.findall("(rect) (\d+)x(\d+)|(rotate) (row|column) [xy]=(\d+) by (\d+)", input)[0] if i]

    if command[0] == "rotate":
        myscreen.rotate(command[1], int(command[2]), int(command[3]))
    elif  command[0] == "rect":
        myscreen.rect(int(command[1]), int(command[2]))
    else:
        raise

print(str(myscreen.count()))
print(myscreen.display)
with open("day08_out.txt", "w") as writer:
    for s in myscreen.display:
        writer.write(str(s).replace("\n", "").replace("1", "#").replace("0", ".") )
        writer.write("\n")
