data = map(lambda x:[int(k[:-1]) if i > 0
                     else [int(z[1:]) for z in k.split("-")[-2:]]
                     for i, k in enumerate(x.split())], open("day22_input").read().split("\n"))


pairs = 0

for i, disk in enumerate(data):

    if disk[2] == 0:
        continue

    for i_, disk_ in enumerate(data):

        if i == i_:
            continue

        if disk[2] <= disk_[3]:
            pairs += 1

print pairs


