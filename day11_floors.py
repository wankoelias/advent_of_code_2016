import re
from collections import defaultdict, Counter
import random
from copy import deepcopy
from datetime import datetime

class builing():

    def __init__(self, input_):
        self.floors_init = defaultdict(list)
        for nr, line in enumerate(open(input_).read().splitlines()):
            self.steps_mem = Counter([999])
            self.Maxsteps = 77

            self.floors_init[nr] += map(lambda x: x.upper()+"G", re.findall("(\w{1})\w+ generator", line))
            self.floors_init[nr] += map(lambda x: x.upper()+"M", re.findall("(\w{1})[-\w]+ microchip", line))
        self.attempts = 0
        self.reset()
        self.best = ""
        self.start = datetime.now()
        self.stamp = self.start

    def reset(self):
        self.floors = deepcopy(self.floors_init)
        self.steps = 0
        self.elevator = 0
        self.solution = ""


    def success(self):
        if sum([len(i) for i in self.floors.values()[:-1]]) == 0:
            return True

    def check_if_valid(self, floor, items=[]):

        for location in [items, floor]:
            if len(location) > 1:
                gens = [i for i in location if i[1] == "G"]
                chips = [i for i in location if i[1] == "M"]

                for g in gens:
                    for c in chips:
                        if g[0] != c[0] and c[0]+"G" not in gens:
                            return False
        return True


    def new_step(self):
        floors_ = deepcopy(self.floors)

        def pick():
            try:
                return random.randint(0, len(self.floors[self.elevator])-1)
            except:
                pass

        # chose direction:
        if self.elevator == 3:
            direct = "d"
        elif self.elevator == 0:
            direct = "u"
        else:
            direct = "u" if random.random() < 0.8 else "d"

        # pick items
        item_nrs = [pick()]
        tr = 0.05 if direct == "u" else 0.99
        # pick another
        if random.random() > tr and len(self.floors[self.elevator]) >= 2:
            second = pick()

            while second in item_nrs:
                second = pick()

            item_nrs.append(second)

        items = [floors_[self.elevator][i] for i in item_nrs]
        for x in items:
            floors_[self.elevator].remove(x)

        if not self.check_if_valid(floors_[self.elevator], items):
            return False

        elevator_ = self.elevator+1 if direct == "u" else self.elevator-1
        floors_[elevator_].extend(items)

        if self.check_if_valid(floors_[elevator_]):
            self.floors = floors_.copy()
            self.elevator = elevator_
            self.steps += 1
            self.log()
            return True


    def new_attempt(self):
        self.reset()
        self.attempts += 1
        counter = 0
        while not self.steps > self.Maxsteps and self.steps <= min(self.steps_mem.keys()):
            if self.success():
                if min(self.steps_mem.keys()) > self.steps:
                    print "new min: {}".format(self.steps)
                    self.best = self.solution
                elif min(self.steps_mem.keys()) == self.steps:
                    print (".")

                self.steps_mem[self.steps] += 1
                return True
            else:
                if self.new_step():
                    counter = 0

                else:
                    counter += 1

                if counter == 20:
                    return False
        return False

    def log(self):
        self.solution += "\nstep: {}".format(self.steps)
        for floor in self.floors.items()[::-1]:
            e = "E" if self.elevator == floor[0] else "."
            self.solution += "\nF{} {} {}".format(floor[0], e, " ".join([x for x in floor[1]]))
        self.solution += "\n"
    def timer(self):
        try:
            print "\t{} attempts".format(self.attempts)
            print "\tduration: {} seconds, avg/s: {}".format( (datetime.now()-self.stamp).seconds, self.attempts/float((datetime.now()-self.start).seconds))
            self.stamp = datetime.now()
        except:
            pass
    def run(self):
        while self.steps_mem[min(self.steps_mem.keys())] < 7:
            self.new_attempt()
            if self.attempts % 1000 == 0:
                self.timer()

        print self.best
        print(str(min(self.steps_mem.keys())))
        self.timer()

bl = builing("day11_input")
bl.run()

