
keys = [[-1, -1, 1, -1, -1], [-1, 2, 3, 4, -1], [5, 6, 7, 8, 9], [-1, "A", "B", "C", -1], [-1, -1, "D", -1, -1]]


input = open("day02_input").read().split("\n")

pos = [2, 0]
password = []
for line in input:
    for l in line:

        if l == "L":
            if pos[1]-1 >= 0:
                if keys[pos[0]][pos[1]-1] != -1:
                    pos[1] -= 1
        elif l  == "R":
            if pos[1]+1 <= 4:
                if keys[pos[0]][pos[1]+1] != -1:
                    pos[1] += 1
        if l == "U":
            if pos[0]-1 >= 0:
                if keys[pos[0]-1][pos[1]] != -1:
                    pos[0] -= 1
        elif l == "D":
            if pos[0]+1 <= 4:
                if keys[pos[0]+1][pos[1]] != -1:
                    pos[0] += 1

        pass

    password.append(keys[pos[0]][pos[1]])

print str(password)