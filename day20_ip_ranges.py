data = open("day20_input").read()
data = map(lambda x: map(int, x.split("-")), data.split("\n"))

data.sort()

count = 0
maxEnd = 0

for n, (start, end) in enumerate(data):
    if n < len(data)-1:
        start_next = data[n+1]
    else:
        start_next = [4294967295]


    if end > maxEnd:
        maxEnd = end

    if maxEnd + 1 < start_next[0]:
        count += start_next[0] - maxEnd - 1
        print "from", maxEnd, "to", start_next[0], start_next[0] - maxEnd - 1


print count