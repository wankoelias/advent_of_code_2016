import re

discs = [map(int, re.findall(".*?\d.*?(\d+).*?\d+.*?(\d+)", x)[0]) for x in open("day15_input").readlines()]

def get_chunk(disc, pos, offset, max):
    chunk = []
    i=0
    while True:
        new = ((disc[0] - disc[1]) - (pos+1)) + ((i+offset)*disc[0])
        if new <= max:
            chunk.append(new)
        else:
            return chunk
        i += 1

common = False
offset = [0]*len(discs)
max = 0
chunksize = 50

while not common:
    max = max+chunksize
    starts = [get_chunk(disc, pos, offset[pos], max) for pos, disc in enumerate(discs)]
    offset = [offset[pos] + len(x) for pos, x in enumerate(starts)]
    for st in starts[0]:
        if all(st in y for y in starts):
            print str(st)
            common = True
            break