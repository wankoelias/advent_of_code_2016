import re

def check_abba(string):
    for i in range(len(string)-3):
        first = string[i:i+2]
        second = string[i+2:i+4]
        if first == second[::-1] and not first == second:
            return True

def get_reverse_abas(parts_out):
    abas = []
    for string in parts_out:
        for i in range(len(string)-2):
            if string[i] == string[i+2] and string[i] is not string[i+1]:
                abas.append(string[i+1]+string[i]+string[i+1])
    return list(set(abas))

input_ = open("day07_input").read().split("\n")

valid_abba_count = 0
valid_bab_count = 0

for i in input_:
    inside_abba_val = True
    outside_abba_val = False

    bab_val = False

    parts_in = re.findall("\[(\w+)\]", i)
    parts_out = re.findall("(\w+)(?:\[\w+\]+)?", i)

    for part_in in parts_in:
        if check_abba(part_in):
            inside_abba_val = False

    for part_out in parts_out:
        if check_abba(part_out):
            outside_abba_val = True

    if inside_abba_val and outside_abba_val:
        valid_abba_count += 1

    abas_rev = get_reverse_abas(parts_out)

    for aba_rev in abas_rev:
        for part_in in parts_in:
            if aba_rev in part_in:
                bab_val = True

    if bab_val:
        valid_bab_count += 1


print str(valid_abba_count)
print str(valid_bab_count)