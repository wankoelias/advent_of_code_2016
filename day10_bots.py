import re

inp = open("day10_input").read()
patterns = ["value (\d+) goes to bot (\d+)", "bot (\d+) gives low to (bot|output) (\d+) and high to (bot|output) (\d+)"]
b_init = [(int(i[0]), int(i[1])) for i in re.findall(patterns[0], inp)]
b_give = [(int(i[0]), i[1], int(i[2]), i[3], int(i[4])) for i in re.findall(patterns[1], inp)]

bots = {}
output = {}
target = set()
for value, bot in b_init:
    bots[bot] = [value] if bot not in bots.keys() else [bots[bot][0], value]

while b_give:
    for i, (s_bot, type_low, nr_low, type_high, nr_high) in enumerate(b_give):

        target = target.union([x for x, v in bots.items() if 61 in v and 17 in v])

        if s_bot not in bots.keys():
            continue
        if len(bots[s_bot]) == 2:
            if type_low == "bot":
                if not nr_low in bots.keys():
                    bots[nr_low] = []
                bots[nr_low].append(min(bots[s_bot]))
            else:
                if not nr_low in output.keys():
                    output[nr_low] = []
                output[nr_low].append(min(bots[s_bot]))
            if type_high == "bot":
                if not nr_high in bots.keys():
                    bots[nr_high] = []
                bots[nr_high].append(max(bots[s_bot]))
            else:
                if not nr_high in output.keys():
                    output[nr_high] = []
                output[nr_high].append(max(bots[s_bot]))
            b_give.pop(i)

print "part 1: %s" %target
print "part 2: %s" % (sum(output[0])*sum(output[1])*sum(output[2]))