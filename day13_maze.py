def wall(x, y, nr=1352):
    if x < 0 or y < 0: return True
    return str(bin(x*x + 3*x + 2*x*y + y + y*y + nr)).count("1") % 2 != 0

start = (1,1)
new, all = [start], [start]
step = 0
target = (31,39)

while target not in new:
    step += 1
    new_ = []
    for n in new:
        x = n[0]
        y = n[1]
        candidates = [i for i in [(x-1 , y), (x+1, y), (x, y-1), (x, y+1)] if i not in all]

        for c in candidates:
            if not wall(c[0], c[1]):
                new_.append(c)
        all.append(n)
    new = new_

""" drawing the field
maxx = max([i[0] for i in all])+3
maxy = max([i[1] for i in all])+3

print "\n step {} \n".format(step)
for y in range(maxy):
    st = ""
    for x in range(maxx):
        if wall(x,y):
            cr = "#"
        elif (x,y) in all:
            cr = "O"
        else:
            cr = "."
        st += cr
    print st
"""

print str(step)