import numpy as np
count = 3014387


elfes = np.arange(count)

current = 0

while count > 1:

    opposite = (current + count / 2) % count

    elfes = np.delete(elfes, opposite)
    count -= 1

    if opposite > current:
        current += 1

    if current == count:
        current = 0

    if count % 1000 == 0:
        print count


print "> ", elfes[0] + 1