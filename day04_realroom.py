input_ = [x.split("-") for x in open("day04_input").read().replace("[","-").replace("]", "").split("\n")]

id_sum = 0

for i in input_:

    toscan="".join(i[:-2])
    mc = {v: toscan.count(v) for v in toscan}
    counts = sorted(list(set(mc.values())), reverse=True)

    checksum =  "".join(["".join(sorted([c for c in mc.keys() if mc[c] == k])) for k in counts])[:5]

    if checksum == i[-1]:
        id_sum += int(i[-2])

        decrypted = "_".join(["".join([chr(((ord(letter)-97+int(i[-2]))%26)+97) for letter in part]) for part in i[:-2]])
        if "north" in decrypted or "pole" in decrypted:
            print i[-2]

            print decrypted

print str(id_sum)