rows = ["^^.^..^.....^..^..^^...^^.^....^^^.^.^^....^.^^^...^^^^.^^^^.^..^^^^.^^.^.^.^.^.^^...^^..^^^..^.^^^^"]

while len(rows) < 400000:
    row = "." + rows[-1] + "."
    new = ""
    for nr, r in enumerate(row[1:-1]):
        new += "^" if row[nr:nr+3] in ["^^.", ".^^", "^..", "..^"] else "."
    rows.append(new)

print str(str(rows).count("."))