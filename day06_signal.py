import numpy as np
input_ = [list(x) for x in open("day06_input").read().split("\n")]
trans = np.array(input_).swapaxes(0,1).tolist()

print "".join([sorted(m.items(), key=lambda u: u[1])[0][0] for m in [{i:x.count(i) for i in set(x)} for x in trans]])