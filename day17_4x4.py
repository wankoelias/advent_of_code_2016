import hashlib
passcode = "rrrbmfta"

def position(pos, path, shortest):

    if pos == [3, 3]:
        return path, len(path)

    options = [x in "bcdef" for x in hashlib.md5(passcode + path).hexdigest()[:4]]

    if pos[1] == 0: options[0] = False
    if pos[1] == 3: options[1] = False
    if pos[0] == 0: options[2] = False
    if pos[0] == 3: options[3] = False

    def move(n, x, y, direct, shortest):
        if options[n]:
            return position([pos[0]+x, pos[1]+y], path+direct, shortest)


    for n, x, y, direct in [(0, 0, -1 , "U"), (1, 0, 1, "D"), (2, -1, 0, "L"), (3, 1, 0, "R")]:
        pn = move(n, x, y, direct, shortest)
        if pn:
            if pn[1] > shortest[1] or not shortest[1]:
                shortest = pn
    return shortest


p = position([0,0], "", [None, None])
print str(p[1])